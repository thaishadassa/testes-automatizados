package br.ce.thaisrezende.appiumC;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class testFlow {

    @Test
    public void alterarIdiomaIng() throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("platformName", "Android");
        cap.setCapability("deviceName", "0049821677");
        cap.setCapability("automationName", "uiautomator2");
        cap.setCapability("appPackage", "com.afferolab.cast");
        cap.setCapability("appActivity", "com.afferolab.cast.MainActivity");

        AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        //Realiza Login
        MobileElement campoNome = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText[1]");
        campoNome.sendKeys("usuario");
        MobileElement campoSenha = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText[2]");
        campoSenha.sendKeys("senha");
        MobileElement botaoAcessar = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup");
        botaoAcessar.click();

        //Selecionar Meu Perfil
        MobileElement meuPerfil = driver.findElement(By.xpath("//android.widget.Button/android.widget.TextView[@text='Meu perfil']"));
        meuPerfil.click();

        //Clicar em Alterar Idioma
        driver.findElement(By.xpath("//android.view.ViewGroup/android.widget.TextView[@text='Alterar idioma do aplicativo']")).click();
        driver.findElement(By.xpath("//android.view.ViewGroup/android.widget.TextView[@text='English']")).click();

        MobileElement home = driver.findElement(By.xpath("//android.widget.TextView[@text='Home']"));
        home.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        String text = driver.findElement(By.xpath("//android.view.ViewGroup/android.widget.TextView[@text='Hello, ']")).getText();
        Assert.assertEquals("Hello, ", text);

        driver.quit();

    }

    @Test
    public void alterarIdiomaPt() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", "Android");
        desiredCapabilities.setCapability("deviceName", "0049821677");
        desiredCapabilities.setCapability("automationName", "uiautomator2");
        desiredCapabilities.setCapability("appPackage", "com.afferolab.cast");
        desiredCapabilities.setCapability("appActivity", "com.afferolab.cast.MainActivity");

        AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        //Realiza Login
        MobileElement campoNome = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText[1]");
        campoNome.sendKeys("dev.afferolab.1@mailinator.com");
        MobileElement campoSenha = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText[2]");
        campoSenha.sendKeys("Casta123!!");
        MobileElement botaoAcessar = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup");
        botaoAcessar.click();

        //Selecionar Meu Perfil
        MobileElement myProfile = driver.findElement(By.xpath("//android.widget.Button/android.widget.TextView[@text='My profile']"));
        myProfile.click();

        //Clicar em Alterar Idioma
        driver.findElement(By.xpath("//android.view.ViewGroup/android.widget.TextView[@text='Change application language']")).click();
        driver.findElement(By.xpath("//android.view.ViewGroup/android.widget.TextView[@text='Português']")).click();

        MobileElement home = driver.findElement(By.xpath("//android.widget.TextView[@text='Home']"));
        home.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        String text = driver.findElement(By.xpath("//android.view.ViewGroup/android.widget.TextView[@text='Olá, ']")).getText();
        Assert.assertEquals("Olá, ", text);

        driver.quit();

    }

}
