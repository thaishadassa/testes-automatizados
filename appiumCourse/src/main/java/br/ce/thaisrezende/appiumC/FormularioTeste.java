package br.ce.thaisrezende.appiumC;


import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FormularioTeste {

    @Test
    public void preencherCampoTextoTest() throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("platformName", "Android");
        cap.setCapability("deviceName", "emulator");
        cap.setCapability("automationName", "uiautomator2");
        cap.setCapability(MobileCapabilityType.APP, "C:\\Users\\thais.rezende\\IdeaProjects\\appiumCourse\\src\\main\\resources\\original.apk");

        AndroidDriver<MobileElement> driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        List<MobileElement> elementosEncontrados = driver.findElements(By.className("android.widget.TextView"));
        elementosEncontrados.get(1).click();

        MobileElement campoNome = driver.findElement(MobileBy.AccessibilityId("nome"));
        campoNome.sendKeys("Thais");

        String text = campoNome.getText();
        Assert.assertEquals("Thais", text);

        //Selecionar formulário
    }


    @Test
    public void interagirComboTest() throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("platformName", "Android");
        cap.setCapability("deviceName", "emulator");
        cap.setCapability("automationName", "uiautomator2");
        cap.setCapability(MobileCapabilityType.APP, "C:\\Users\\thais.rezende\\IdeaProjects\\appiumCourse\\src\\main\\resources\\original.apk");

        AndroidDriver<MobileElement> driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Selecionar Formulário
        driver.findElement(By.xpath("//android.widget.TextView[@text='Formulário']")).click();

        //Escolher uma opção do combo;
        driver.findElement(MobileBy.AccessibilityId("console")).click();
        driver.findElement(By.xpath("//android.widget.ListView/android.widget.CheckedTextView[@text='PS4']")).click();

        String opcao = driver.findElement(By.xpath("//android.widget.Spinner/android.widget.TextView")).getText();
        Assert.assertEquals("PS4", opcao);

        driver.quit();

//        MobileElement campoNome = driver.findElement(MobileBy.AccessibilityId("nome"));
//        campoNome.sendKeys("Thais");
//
//        String text = campoNome.getText();
//        Assert.assertEquals("Thais", text);

        //Selecionar formulário
    }
        @Test
        public void deveInteragirSwitchCheckBox() throws MalformedURLException {

            DesiredCapabilities cap = new DesiredCapabilities();
            cap.setCapability("platformName", "Android");
            cap.setCapability("deviceName", "emulator");
            cap.setCapability("automationName", "uiautomator2");
            cap.setCapability(MobileCapabilityType.APP, "C:\\Users\\thais.rezende\\IdeaProjects\\appiumCourse\\src\\main\\resources\\original.apk");

            AndroidDriver<MobileElement> driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            //Selecionar Formulário
            driver.findElement(By.xpath("//android.widget.TextView[@text='Formulário']")).click();

            //Escolher uma opção do combo;
            driver.findElement(MobileBy.AccessibilityId("console")).click();
            driver.findElement(By.xpath("//android.widget.ListView/android.widget.CheckedTextView[@text='PS4']")).click();

            String opcao = driver.findElement(By.xpath("//android.widget.Spinner/android.widget.TextView")).getText();
            Assert.assertEquals("PS4", opcao);

            driver.quit();

            MobileElement campoNome = driver.findElement(MobileBy.AccessibilityId("nome"));
            campoNome.sendKeys("Thais");

            String text = campoNome.getText();
            Assert.assertEquals("Thais", text);
    }
}
