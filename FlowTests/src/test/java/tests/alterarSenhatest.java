package tests;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

public class alterarSenhatest {

        private WebDriver navegador;
        private LocalStorage localStorage;

        @Before
        public void setUp() {
            //Abrindo o navegador
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\thais.rezende\\OneDrive - Afferolab Participações SA\\Área de Trabalho\\QA\\SGL\\chromedriver.exe");
            navegador = new ChromeDriver();
            navegador.manage().window().maximize();
            navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            navegador.get("https://stag-flow.afferolab.net/qa");
        }
        @Test
        public void resetSenha() throws InterruptedException {
            String email = "testeflowqa@mailinator.com";
            navegador.findElement(By.xpath("//form[@class='form-container needs-validation']//button[@class='btn btn-link btn-sm btn-block m-top--sm']")).click();
            navegador.findElement(By.id("emailInput")).sendKeys(email);
            navegador.findElement(By.xpath("//button[@type='submit']")).click();
            navegador.findElement(By.xpath("//button[@type='button']")).click();

            WebDriver driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            driver.get("https://mailinator.com");
            driver.findElement(By.id("addOverlay")).sendKeys(email);
            driver.findElement(By.id("go-to-public")).click();
            driver.findElement(By.xpath("//a[contains(text(),'Esqueceu sua senha?')]")).click();
            Thread.sleep(1000);
            driver.switchTo().frame("msg_body");
            WebElement elemento = driver.findElement(By.xpath("/html/body/div/table/tbody/tr[7]/td/table/tbody/tr/td[2]/table/tbody/tr/td/span/font"));
            String senha = elemento.getText();
            driver.quit();
            navegador.findElement(By.id("passwordInput")).sendKeys(senha);
            navegador.findElement(By.xpath("//button[@type='submit']")).click();
            Thread.sleep(1000);
            WebElement modalAlteracaoSenha = navegador.findElement(By.xpath("//div[@class='change-modal-content modal-content']"));
            modalAlteracaoSenha.findElement(By.xpath("//*[@id='titleInput' and @placeholder='Senha atual']")).sendKeys(senha);
            modalAlteracaoSenha.findElement(By.xpath("//*[@id='titleInput' and @placeholder='Nova senha']")).sendKeys("Flow123@");
            modalAlteracaoSenha.findElement(By.xpath("//*[@type='password' and @placeholder='Repita a nova senha']")).sendKeys("Flow123@");
            modalAlteracaoSenha.findElement(By.xpath("//div[@class='modal-confirm-button']/button")).click();
            By.className("message success");
            boolean isElementPresent = driver.findElement(By.className("message success"));
            assertEquals("message success", (navegador.findElement(By.className("message success"))).getText());

            navegador.findElement(By.xpath("//button[@class='btn btn-link btn-avatar']/i[@class='material-icons va-m down']")).click();
            WebDriverWait wait = new WebDriverWait(navegador, 10);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='title']")));
            navegador.findElement(By.xpath("//a[@href='#/logout']")).click();
            navegador.findElement(By.id("emailInput")).sendKeys(email);
            navegador.findElement(By.id("passwordInput")).sendKeys(senha);
            navegador.findElement(By.xpath("//button[@type='submit']")).click();
            WebElement msg = navegador.findElement(By.xpath("//p[@class='alert alert-danger m--none p--xxs']"));
            String esperado = msg.getText();
            assertEquals("Usuário e/ou senha incorretos!",esperado);
            navegador.quit();

        }
        @Test
        public void emailInvalidoResetPttest() {
            navegador.findElement(By.xpath("//form[@class='form-container needs-validation']//button[@class='btn btn-link btn-sm btn-block m-top--sm']")).click();
            navegador.findElement(By.id("emailInput")).sendKeys("testeresetflow@mailinator.com");
            navegador.findElement(By.xpath("//button[@type='submit']")).click();
            WebElement texto = navegador.findElement(By.xpath("//*[@id=\"root\"]/div[2]/div/div/form/p"));
            String esperado = texto.getText();
            assertEquals("Seu e-mail não está cadastrado no sistema. Tente com outro endereço ou entre em contato com o suporte.", esperado);
        }
        @Test
        public void emailInvalidoResetIntest() {
            navegador.findElement(By.xpath("//form[@class='form-container needs-validation']//button[@class='btn btn-link btn-sm btn-block m-top--sm']")).click();
            navegador.findElement(By.id("emailInput")).sendKeys("testeresetflow@mailinator.com");
            navegador.findElement(By.xpath("//button[@type='submit']")).click();
//            WebElement texto = navegador.findElement(By.xpath("//*[@id=\"root\"]/div[2]/div/div/form/p"));
//            String esperado = texto.getText();
            assertEquals("message success", (navegador.findElement(By.className("message success"))).getText());
        }
}

