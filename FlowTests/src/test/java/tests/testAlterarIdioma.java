package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class testAlterarIdioma {

    private WebDriver navegador;
    @Before
    public void setUp() {
        //Abrindo o navegador
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\thais.rezende\\OneDrive - Afferolab Participações SA\\Área de Trabalho\\QA\\SGL\\chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        navegador.get("https://stag-flow.afferolab.net/qa");
    }

    @Test
    public void alterarIdiomaWeb() throws InterruptedException {
        //Realizar Login
        navegador.findElement(By.id("emailInput")).sendKeys("usuario");
        navegador.findElement(By.id("passwordInput")).sendKeys("Senha");
        navegador.findElement(By.xpath("//button[@type='submit']")).click();
        Thread.sleep(2000);
        WebStorage webStorage = (WebStorage) new Augmenter().augment(navegador);
        LocalStorage localStorage = webStorage.getLocalStorage();

        // Alterar Idioma
        if(localStorage.getItem("castLocale") != null && localStorage.getItem("castLocale").equalsIgnoreCase("pt")) {
            // Expandir opções
            navegador.findElement(By.xpath("//button[@class='btn btn-link btn-avatar']/i[@class='material-icons va-m down']")).click();

            // Aguardar a exibição das opções
            WebDriverWait wait = new WebDriverWait(navegador, 10);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='title']")));

            navegador.findElement(By.xpath("//div[@class='dropdown-menu dropdown-menu-right show']//span[@class='title' and .='Alterar idioma']")).click();

            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='change-modal-content modal-content']")));
            WebElement modalAlteracao = navegador.findElement(By.xpath("//div[@class='change-modal-content modal-content']"));

            WebDriverWait wait1 = new WebDriverWait(navegador,30);
            WebElement opcaoIngles = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='change-modal-content modal-content']/div[@class='option-container']/span")));
            opcaoIngles.click();

            WebElement botaoConfirmacao = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@data-dismiss='modal']")));
            botaoConfirmacao.click();

            String idioma = localStorage.getItem("castLocale");
            assertEquals("en", idioma);
        }
        else {
            // Expandir opções
            navegador.findElement(By.xpath("//button[@class='btn btn-link btn-avatar']/i[@class='material-icons va-m down']")).click();

            // Aguardar a exibição das opções
            WebDriverWait wait = new WebDriverWait(navegador, 10);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='title']")));

            navegador.findElement(By.xpath("//div[@class='dropdown-menu dropdown-menu-right show']//span[@class='title' and .='Change language']")).click();

            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='change-modal-content modal-content']")));
            WebElement modalAlteracao = navegador.findElement(By.xpath("//div[@class='change-modal-content modal-content']"));

            WebDriverWait wait1 = new WebDriverWait(navegador,30);
            WebElement opcaoPortuges = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='change-modal-content modal-content']//div[@class='option-container'][2]/span")));
            opcaoPortuges.click();

            WebElement botaoConfirmacao = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@data-dismiss='modal']")));
            botaoConfirmacao.click();
            String idioma = localStorage.getItem("castLocale");
            assertEquals("en", idioma);
        }
    }
    @After
    public void tearDown() {
        navegador.quit(); }
}
