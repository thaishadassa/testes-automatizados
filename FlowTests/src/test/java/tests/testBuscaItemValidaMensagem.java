package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class testBuscaItemValidaMensagem {
    private WebDriver navegador;

    @Before
    public void setUp() {
        //Abrindo o navegador
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\thais.rezende\\OneDrive - Afferolab Participações SA\\Área de Trabalho\\QA\\SGL\\chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        navegador.get("https://stag-flow.afferolab.net/qa");
    }

    @Test
    public void validaMensagemItem() throws InterruptedException {
        //Realizar Login
        navegador.findElement(By.id("emailInput")).sendKeys("usuario");
        navegador.findElement(By.id("passwordInput")).sendKeys("senha");
        navegador.findElement(By.xpath("//button[@type='submit']")).click();
        Thread.sleep(2000);
        WebStorage webStorage = (WebStorage) new Augmenter().augment(navegador);
        LocalStorage localStorage = webStorage.getLocalStorage();

        if (localStorage.getItem("castLocale").equalsIgnoreCase("en")) {

            navegador.findElement(By.xpath("//a[@href='#/items']")).click();
            navegador.findElement(By.xpath("//input[@type='search']")).sendKeys("Inexistente");
            WebDriverWait wait = new WebDriverWait(navegador, 10);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//div[@class='empty-item']/h1)[1]")));
            WebElement texto = navegador.findElement(By.xpath("(//div[@class='empty-item']/h1)[1]"));
            String esperado = texto.getText();
            assertEquals("No items match your search!", esperado);
        }
        else if(localStorage.getItem("castLocale").equalsIgnoreCase("pt")) {

            navegador.findElement(By.xpath("//a[@href='#/items']")).click();
            navegador.findElement(By.xpath("//input[@type='search']")).sendKeys("Inexistente");
            WebDriverWait wait = new WebDriverWait(navegador, 10);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//div[@class='empty-item']/h1)[1]")));
            WebElement texto = navegador.findElement(By.xpath("(//div[@class='empty-item']/h1)[1]"));
            String esperado = texto.getText();
            assertEquals("Não existem itens correspondentes à sua busca!", esperado);
        }
    }

    @After
    public void tearDown() {
        navegador.quit();
    }

}
