package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LoginLogouttest {
    private WebDriver navegador;

    @Before
    public void setUp() {
        //Abrindo o navegador
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\thais.rezende\\OneDrive - Afferolab Participações SA\\Área de Trabalho\\QA\\SGL\\chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        navegador.get("https://stag-flow.afferolab.net/qa");
    }
    @Test
    public void testRealizarLogin(){
        navegador.findElement(By.id("emailInput")).sendKeys("usuario");
        navegador.findElement(By.id("passwordInput")).sendKeys("Senha");
        navegador.findElement(By.xpath("//button[@type='submit']")).click();
        WebElement texto = navegador.findElement(By.xpath("//header//a[@aria-current='page']"));
        String esperado = texto.getText();
        assertEquals("Home",esperado);
    }
    @Test
    public void testRealizarLogout() throws InterruptedException {

            navegador.findElement(By.id("emailInput")).sendKeys("Usuario");
            navegador.findElement(By.id("passwordInput")).sendKeys("Senha");
            navegador.findElement(By.xpath("//button[@type='submit']")).click();
            Thread.sleep(2000);
            WebStorage webStorage = (WebStorage) new Augmenter().augment(navegador);
            LocalStorage localStorage = webStorage.getLocalStorage();
            navegador.findElement(By.xpath("//button[@class='btn btn-link btn-avatar']/i[@class='material-icons va-m down']")).click();
            WebDriverWait wait = new WebDriverWait(navegador, 10);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='title']")));
            navegador.findElement(By.xpath("//a[@href='#/logout']")).click();

        if (localStorage.getItem("castLocale") != null && localStorage.getItem("castLocale").equalsIgnoreCase("pt")) {
            WebElement texto = navegador.findElement(By.xpath("//div[@id='root']//h1"));
            String esperado = texto.getText();
            assertEquals("Bem-vindo ao", esperado);
        }
        else {
            WebElement texto = navegador.findElement(By.xpath("//div[@id='root']//h1"));
            String esperado = texto.getText();
            assertEquals("Welcome to the", esperado);
        }
    }
    @After
    public void tearDown() {
        navegador.quit();
    }
}
