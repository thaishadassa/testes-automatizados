package tests;

import static org.junit.Assert.*;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OpcoesUsuario {
    @Test
    public void AlterarIdiomaPtInTest(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\thais.rezende\\Desktop\\QA\\SGL\\chromedriver.exe");
        WebDriver navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.get("https://stag-flow.afferolab.net/qa");

        new Espera(driver)
                .aguardarElemento("emailInput");
        navegador.findElement(By.id("emailInput")).sendKeys("usuario");
        navegador.findElement(By.id("passwordInput")).sendKeys("senha");
        navegador.findElement(By.xpath("//button[@type='submit']")).click();
        espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button/i[@class=\"material-icons va-m down\"]")));

        navegador.findElement(By.xpath("//button/i[@class=\"material-icons va-m down\"]")).click();
        navegador.findElement(By.xpath(("(//div[@class=\"menu-item\"]/span[@class=\"title\"])[3]"))).click();
        espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//div[@class=\"option-container\"]/span[@class=\"null\"])[1]")));
        navegador.findElement(By.xpath("(//div[@class=\"option-container\"]/span[@class=\"null\"])[1]")).click();

    }
}
